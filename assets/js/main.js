const navbar = document.querySelector("#navbar");
const navbarHamburger = navbar.querySelector("#navbar-hamburger");
const navbarLinks = navbar.querySelector("#navbar-links");
const navbarDrawer = navbar.querySelector("#navbar-drawer");
const children = [...document.querySelector("#sections").children];
const homeSection = document.querySelector(`.section[name="#home"]`);

if (window.pageYOffset < homeSection.offsetHeight) {
  navbar.style.backdropFilter = "none";
  navbarDrawer.style.backdropFilter = "none";
} else {
  navbar.style.backdropFilter = "blur(5px)";
  navbarDrawer.style.backdropFilter = "blur(5px)";
}

let lastHashElement = null;

[
  ...navbarLinks.querySelectorAll("a"),
  ...navbarDrawer.querySelectorAll("a"),
].forEach(function (e) {
  const section = document.querySelector(
    `.section[name="${e.getAttribute("href")}"]`
  );
  e.addEventListener("click", function () {
    window.scroll({
      top: section.offsetTop - 100,
      behavior: "smooth",
    });
  });
});

navbarHamburger.addEventListener("click", function () {
  navbar.classList.toggle("open");

  if (navbar.classList.contains("open")) {
    document.body.style.overflowY = "hidden";
  } else {
    document.body.style.overflowY = "auto";
  }
});

window.addEventListener("resize", function () {
  if (window.innerWidth <= 800 && navbar.classList.contains("open")) {
    document.body.style.overflowY = "hidden";
  } else {
    document.body.style.overflowY = "auto";
  }
});

window.addEventListener("hashchange", hashHandler);

function hashHandler() {
  if (window.location.hash == "") {
    window.location.hash = "#home";
  }
}

hashHandler();

window.addEventListener("scroll", function () {
  const y = Math.ceil(window.pageYOffset);

  if (y < homeSection.offsetHeight) {
    navbar.style.backdropFilter = "none";
    navbarDrawer.style.backdropFilter = "none";
  } else {
    navbar.style.backdropFilter = "blur(5px)";
    navbarDrawer.style.backdropFilter = "blur(5px)";
  }

  const section = children.filter((e) => y >= e.offsetTop - 120);

  if (section) {
    const last = section.pop();
    const name = last.getAttribute("name");
    const link = navbarLinks.querySelector(`[href="${name}"]`);
    if (link) {
      if (lastHashElement != null) {
        lastHashElement.classList.remove("active");
      }
      link.classList.add("active");
      lastHashElement = link;
      window.location.hash = name;
    }
  }
});
